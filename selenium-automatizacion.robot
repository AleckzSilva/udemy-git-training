*** Settings ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${Browser}      Chrome
${HomePage}     automationpractice.com/index.php
${Scheme}       http
${ProdScheme}   https
${QAUrl}      ${Scheme}://${HomePage}
${ProdUrl}      ${ProdScheme}://${HomePage}

*** Keywords ***
Open HomePage
    Open Browser    ${QAUrl}    ${Browser}

*** Test Cases ***
C001 - Hacer clic en contenedores
    Open HomePage
    Set Global Variable     @{nombresdecontenedores}     //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    FOR    ${nombredecontenedor}   IN      @{nombresdecontenedores}
    Wait Until Element Is Visible       xpath=${nombredecontenedor}
       Click Element       xpath=${nombredecontenedor}
       Wait Until Element Is Visible       xpath=//*[@id="bigpic"]
       Click Element       xpath=//*[@id="header_logo"]/a/img
    END
    Close Browser

C002 - Nuevo Caso de Prueba
    Open HomePage